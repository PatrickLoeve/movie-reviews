# README

Getting started
To get started with the app, first clone the repo and cd into the directory:
$ cd movie-reviews

Then install the needed gems (while skipping any gems needed only in production):
$ bundle install --without production

Install JavaScript dependencies:
$ yarn install

Next, migrate the database:
$ rails db:migrate

Next import the csv files to the database:
$ rake csv_import:movies

Finally, run the test suite to verify that everything is working correctly
$ rails rspec spec

If the test suite passes, you'll be ready to run the app in a local server:

$ rails server