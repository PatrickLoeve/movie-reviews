class ActorsController < ApplicationController
  def index
    @actors = Actor.search(params[:search])
  end
end
