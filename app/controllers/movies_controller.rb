class MoviesController < ApplicationController

  def index
      @movies = Movie.all
      @movies.includes(:reviews).sort_by(&:average_rating).reverse
  end
end
