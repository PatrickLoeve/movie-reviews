class Review < ApplicationRecord
    belongs_to :user
    belongs_to :movie

    validates :comment, :stars, :movie_id, :user_id, presence: true
end
