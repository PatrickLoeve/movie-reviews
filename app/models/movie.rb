class Movie < ApplicationRecord
  has_and_belongs_to_many :actors
  has_and_belongs_to_many :locations
  has_many :reviews
  belongs_to :director

  validates :title, :description, :year, :director_id, presence: true

  def average_rating
    ratings = reviews.map(&:stars)
    ratings.sum.to_f / ratings.size
  end
end
