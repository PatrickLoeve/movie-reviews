require 'rails_helper'

RSpec.describe "Movies", type: :request do
  describe "GET /index" do
    it 'renders the index template for a overview of all movies ' do
      get '/'
      expect(response).to render_template(:index)
    end
  end
end
