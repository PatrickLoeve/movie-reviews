require 'rails_helper'

RSpec.describe Review, type: :model do

  it { should belong_to(:user) }
  it { should belong_to(:movie) }

  subject {
    user = User.create(username: 'Kees')
    director = Director.create(name: 'Valentino')
    movie = Movie.create(title: 'test movie',
                      description: 'this is a movie is about....',
                      year: 2020,
                      director_id: director.id)
    described_class.new(comment: 'this movie is amazing,,,,',
                        stars: 3,
                        movie_id: movie.id,
                        user_id: user.id)
  }

  describe 'create' do
    it 'should be valid' do
      expect(subject).to be_valid
    end

    it 'should not be valid without movie_id' do
      subject.movie_id = nil
      expect(subject).not_to be_valid
    end
  end
end
