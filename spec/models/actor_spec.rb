require 'rails_helper'

RSpec.describe Actor, type: :model do
  context "associations" do
    it { should have_and_belong_to_many(:movies)}

    let(:actor) { Actor.create(name: 'Nicolas Cage')}

    it 'finds the searched actor' do
      result = Actor.search('Nicolas')
      expect(result).to eq([actor])
    end
  end
end
