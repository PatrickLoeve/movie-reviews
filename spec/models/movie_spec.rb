require 'rails_helper'

RSpec.describe Movie, type: :model do

  subject {
    director = Director.create(name: 'Patrick')
    described_class.new(title: 'test movie',
                        description: 'this is a movie is about....',
                        year: 2020,
                        director_id: director.id)
  }

  context "associations" do
    it { should have_many(:reviews) }
    it { should have_and_belong_to_many(:locations) }
    it { should belong_to(:director) }
    it { should have_and_belong_to_many(:actors)}
  end

  describe 'create' do
    it 'should be valid' do
      expect(subject).to be_valid
    end

    it 'should not be valid without director_id' do
      subject.director_id = nil
      expect(subject).not_to be_valid
    end
  end

  describe 'average star score' do
    let(:reviews) {
      reviews = [
        Review.create(stars: 2, comment: 'text 1', movie_id: 1),
        Review.create(stars: 4, comment: 'text 2', movie_id: 1),
        Review.create(stars: 3, comment: 'text 3', movie_id: 1)
      ]
    }

    before do
      allow(subject).to receive(:reviews).and_return reviews
    end

    it 'is the average of the review rating stars' do
      expect(subject.average_rating).to eq(3)
    end
  end
end
