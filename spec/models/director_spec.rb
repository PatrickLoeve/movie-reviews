require 'rails_helper'

RSpec.describe Director, type: :model do
  context "associations" do
    it { should have_many(:movies) }
  end
end
