require 'rails_helper'

RSpec.describe Location, type: :model do
  context "associations" do
    it { should have_and_belong_to_many(:movies) }
  end
end
