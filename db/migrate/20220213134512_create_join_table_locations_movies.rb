class CreateJoinTableLocationsMovies < ActiveRecord::Migration[6.0]
  def change
    create_join_table :locations, :movies do |t|
      # t.index [:location_id, :movie_id]
      # t.index [:movie_id, :location_id]
    end
  end
end
