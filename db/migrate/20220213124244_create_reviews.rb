class CreateReviews < ActiveRecord::Migration[6.0]
  def change
    create_table :reviews do |t|
      t.text :comment
      t.integer :stars
      t.belongs_to :user, index: true 
      t.belongs_to :movie, index: true 
      t.timestamps
    end
  end
end
