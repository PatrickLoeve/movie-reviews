namespace :csv_import do
    desc 'seeds csv from db/seed_files to database'
    task movies: :environment do
      require 'csv'
      items = Dir['lib/seed_files/*.csv'].sort_by(&:length)

      items.each do |item|
        CSV.foreach(item, headers: true) do |row|
          row = row.to_hash
          if item == 'lib/seed_files/movies.csv'
            director = Director.where(name: row['Director']).first_or_create
            movie =  Movie.find_or_create_by(title: row['Movie']) do |movie|
              movie.description = row['Description'],
              movie.year = row['Year'],
              movie.director_id =  director.id
            end

            location = Location.where(
                name: row['Filming location'],
                country: row['Country']
            ).first_or_create

            movie.locations << location

            actor = Actor.where(
              name: row['Actor']
            ).first_or_create

            movie.actors << actor
          else
            movie = Movie.find_by(title: row['Movie'])
            User.create!(username: row['User'])
            user = User.find_by(username: row['User'])
            Review.find_or_create_by(movie_id: movie.id, user_id: user.id, stars: row['Stars'],comment: row['Review'])

          end
        end
      end
      puts('Data is imported')
    end
  end
